import React, { useLayoutEffect } from 'react'
import { useNavigation } from '@react-navigation/native'
import { useSelector } from 'react-redux'
import NoteItem from '../../components/NoteItem/index'

import {
    Container,
    AddButton,
    AddButtonImage,
    NotesList,
    NoNotes,
    NoNotesImage,
    NoNotesText
} from './styles'


export default () => {
    const navigation = useNavigation()
    const list = useSelector(state => state.notes.list)


    useLayoutEffect(() => { //apresenta um layout dependendo da decisão
        navigation.setOptions({
            titile: 'Suas notas',
            headerRight: () => (
                <AddButton underlayColor="#0033ee" onPress={() => navigation.navigate('EditNote')}>
                    <AddButtonImage source={require('../../assets/more.png')} />
                </AddButton>
            )
        })
    }, [])

    const handleNotePress = (index) => {
        navigation.navigate('EditNote', {
            key: index
        })
    }

    return (
        <Container>
            {list.length > 0 &&
                <NotesList
                    data={list}
                    renderItem={({ item, index }) => (
                        <NoteItem
                            data={item}
                            index={index}
                            onPress={handleNotePress}
                        />
                    )}
                    keyExtractor={(item, index) => index.toString()}
                />
            }

            {list.length == 0 &&
                <NoNotes>
                    <NoNotesImage source={require('../../assets/note.png')} />
                    <NoNotesText>Nenhuma nota até o momento</NoNotesText>
                </NoNotes>
            }
        </Container>
    )
}
